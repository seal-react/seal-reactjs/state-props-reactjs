import { useState } from "react";
import logo from "./logo.svg";
import TodoList from "./components/TodoList";
import "./App.css";

function App() {
  const [list, setList] = useState([]);
  const [input, setInput] = useState("");

  const onSave = () => {
    const newList = list;
    newList.push(input);

    setList(newList);
    setInput("");
  };

  return (
    <div className="App">
      <input
        type="text"
        placeholder="Tambah Task"
        value={input}
        onChange={(event) => setInput(event.target.value)}
      />
      <button onClick={() => onSave()}>Simpan</button>
      <TodoList list={list} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer">
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
